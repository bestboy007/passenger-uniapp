/*
 * @Author: ch cwl_ch@163.com
 * @Date: 2022-12-27 14:52:17
 * @LastEditors: ch
 * @LastEditTime: 2022-12-30 16:33:24
 * @Description: 字典表
 */
const ORDER_STATUS = {
    // 乘客发起订单
    INIT : 1,
    // 司机接单
    PROCESS: 2,
    // 司机去接乘客
    PROCESS: 2,
    // 司机到底上车点
    ARRIVE_AGREE_ADDR: 3,
    // 行程开始，乘客上车
    ONCAR: 4,
    // 行程结束，到达目的地
    ARRIVED: 5,
    // 发起收款，待支付
    WAIT_PAY: 6,
    // 付款完成，订单完成
    PAYED: 7,
    SCORED: 8,
    EXCEPTION: 10,
    // 订单取消
    CANCELED: 9
    
}
export {
    ORDER_STATUS
}