/*
 * @Author: ch cwl_ch@163.com
 * @Date: 2022-12-19 15:33:58
 * @LastEditors: ch
 * @LastEditTime: 2022-12-30 16:48:20
 * @Description: file content
 */
import { MsbRequest } from "../plugins/requset";

const ApiGetUserProgressOrder = () => MsbRequest.get("/UserProgressOrder");

/**
 * @Description: 获取价格
 * @param {*} 
 * @return {*}
 */
const ApiGetPrice = (
  params = {
    originLng, //城市编码
    originLat, //出发地纬度
    destLng, //目的地纬度
    destLat, //目的地纬度
    areaCode, //城市编码
    vehicleType, //车辆类型
  }
) => MsbRequest.post("/wechat/orderDrivingRouteCalcPre", params);

/**
 * @Description:乘客下单
 * @return {*}
 */
const ApiPostOrderAdd = (
  data = {
    originLng,
    originLat,
    originAddr,
    destLng,
    destLat,
    destAddr
  }
) => MsbRequest.post("/wechat/portal/doDealOrder", data);

/**
 * @Description: 乘客取消订单
 * @param {*} orderId
 * @return {*}
 */
const ApiPostOrderCancel = ({orderNo}) => MsbRequest.post('/wechat/portal/doCancelOrderH5',{orderNo},{
  'content-type':'application/x-www-form-urlencoded'
});
/**

 * @param {*} orderId
 * @return {*}
 */
const GetOrderCalcInfo = ({orderNo}) => MsbRequest.post('/wechat/portal/orderCalcInfo',{orderNo},{
  'content-type':'application/x-www-form-urlencoded'
});
/**
 * @Description: 查询当前用户正在进行的订单
 * @return {*}
 */
const ApiGetCurrentOrder = () => MsbRequest.get('/wechat/portal/unfinishedOrder');

export { 
    ApiGetUserProgressOrder, 
    ApiGetPrice, 
    ApiPostOrderAdd,
    ApiPostOrderCancel,
    ApiGetCurrentOrder,
	GetOrderCalcInfo
};
